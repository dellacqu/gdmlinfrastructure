/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/


#include "GeoModelKernel/GeoVGeometryPlugin.h"

#include "GeoModelXMLParser/XercesParser.h"
#include "GeoModelKernel/GeoNameTag.h"
#include "GeoModelKernel/Units.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoNameTag.h"
#include "GDMLInterface/GDMLController.h"
#define SYSTEM_OF_UNITS GeoModelKernelUnits // so we will get, e.g., 'GeoModelKernelUnits::cm'

#include <iostream>

class GDMLPlugin : public GeoVGeometryPlugin  {

 public:

  // Constructor:
  GDMLPlugin();

  // Destructor:
  ~GDMLPlugin();

  // Creation of geometry:
  virtual void create(GeoPhysVol *world, GeoVStore* store);

 private:

  // Illegal operations:
  const GDMLPlugin & operator=(const GDMLPlugin &right)=delete;
  GDMLPlugin(const GDMLPlugin &right) = delete;

};





GDMLPlugin::GDMLPlugin()
{
}


GDMLPlugin::~GDMLPlugin()
{
}


//## Other Operations (implementation)
void GDMLPlugin::create(GeoPhysVol *world, GeoVStore*)
{

  	std::cout<< "creating a GDMLController and the XercesParser"<<std::endl;
    GDMLController controller("GDMLController");
	  XercesParser xercesParser;
  	xercesParser.ParseFileAndNavigate("gdmlfile.xml");
	GeoPhysVol* w=controller.getWorld();
	GeoNameTag* gdmlTag=new GeoNameTag("GDML setup");
	world->add(gdmlTag);
	world->add(w);
}

extern "C" GDMLPlugin *createGDMLPlugin() {
  return new GDMLPlugin;
}
